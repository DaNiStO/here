/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.here.danielstolero.here.viewmodel;

import android.app.Application;
import android.util.Log;

import com.here.danielstolero.here.DataRepository;
import com.here.danielstolero.here.MyApplication;
import com.here.danielstolero.here.db.entities.TaxiEntity;
import com.here.danielstolero.here.models.Taxi;

import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;

public class TaxiViewModel extends AndroidViewModel {

    // MediatorLiveData can observe other LiveData objects and react on their emissions.
    private final MediatorLiveData<List<TaxiEntity>> mObservableTaxis;

    private final MutableLiveData<Taxi> mIsFinish;

    public TaxiViewModel(Application application) {
        super(application);

        mObservableTaxis = new MediatorLiveData<>();
        // set by default null, until we get data from the database.
        mObservableTaxis.setValue(null);

        LiveData<List<TaxiEntity>> taxis = ((MyApplication) application).getRepository().getTaxis();

        // observe the changes of the taxis from the database and forward them
        mObservableTaxis.addSource(taxis, mObservableTaxis::setValue);

        mIsFinish = new MutableLiveData<>();
        mIsFinish.setValue(null);

        startRandomData();
    }

    /**
     * Expose the LiveData Products query so the UI can observe it.
     */
    public LiveData<List<TaxiEntity>> getTaxis() {
        return mObservableTaxis;
    }

    public LiveData<Taxi> isFinish() {
        return mIsFinish;
    }

    private void startRandomData() {
        MyApplication application = this.getApplication();
        DataRepository repository = application.getRepository();

        final int[] counter = {0};

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                Taxi taxi = repository.isFinish();
                if (taxi != null) {
                    this.cancel();
                    mIsFinish.setValue(taxi);
                }

                Log.d("Daniel", "task working " + counter[0] + " times!!!");

                List<TaxiEntity> data = repository.getTaxis().getValue();

                if(data != null) {
                    Random random = new Random();
                    int changes = random.nextInt(data.size());
                    int [] randomIndexes = new int[changes];
                    int [] ids = new int[changes];
                    for (int i = 0; i < changes; i++) {
                        randomIndexes[i] = random.nextInt(data.size());
                        ids[i] = data.get(randomIndexes[i]).getId();
                    }
                    repository.updateEta(ids);
                }

                counter[0]++;
            }
        };

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(task, TimeUnit.SECONDS.toMillis(5), TimeUnit.SECONDS.toMillis(5));
    }

    public void restart() {
        MyApplication application = this.getApplication();
        application.getDatabase().restart(application.getAppExecutors());
        startRandomData();
    }

}
