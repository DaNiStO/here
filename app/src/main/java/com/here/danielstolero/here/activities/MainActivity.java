package com.here.danielstolero.here.activities;

import android.os.Bundle;

import com.here.danielstolero.here.R;
import com.here.danielstolero.here.adapters.TaxiAdapter;
import com.here.danielstolero.here.base.BaseActivity;
import com.here.danielstolero.here.models.Taxi;
import com.here.danielstolero.here.viewmodel.TaxiViewModel;

import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends BaseActivity {

    private RecyclerView mRecyclerView;
    private TaxiAdapter mAdapter;

    private TaxiViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected int setLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected void initViews() {
        mRecyclerView = findViewById(R.id.taxi_list);
        viewModel = ViewModelProviders.of(this).get(TaxiViewModel.class);
    }

    @Override
    protected void initListeners() {

    }

    @Override
    protected void initData() {
        mAdapter = new TaxiAdapter();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        if (mRecyclerView.getItemAnimator() != null)
            mRecyclerView.getItemAnimator().setChangeDuration(0);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);

        subscribeUi();
    }

    private void subscribeUi() {
        // Update the list when the data changes
        viewModel.getTaxis().observe(this, taxis -> mAdapter.setTaxis(taxis));

        viewModel.isFinish().observe(this, this::showFinishDialog);
    }

    private void showFinishDialog(Taxi taxi) {
        if (taxi != null) {
            new AlertDialog.Builder(MainActivity.this)
                    .setMessage(String.format("The Taxi %s here!!!", taxi.getName()))
                    .setPositiveButton("Finish", (dialog, which) -> finish())
                    .setNegativeButton("Restart", (dialog, which) -> {
                                viewModel.restart();
                            }
                    )
                    .show();
        }
    }
}
